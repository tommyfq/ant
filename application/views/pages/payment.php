<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
                    <div class="tabs-container style-1">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title">Product info</div>
                            <div class="list">
                                <a class="tab-switcher active">Payment </a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-3 information-entry">
                                            <h4>Order Detail</h4>
                                            <p>Seller Name  : <?php echo $username ?></p>
                                            <p>Order Date   : <?php echo $tanggal_order ?></p>
                                            <p><b style="color:#fff">|</b></p>
                                            <p><b>Product Name</b></p>
                                            <p><b><?php echo $nama_produk ?></b></p>
                                            <p style="color:#fff">-</p>
                                            <p><b>Destination</b></p>
                                            <p><b><?php echo $this->session->userdata('username') ?></b></p>
                                            <p><?php echo $alamat ?></p>
                                        </div>
                                        <div class="col-md-3 information-entry">
                                            <h4 style="color:#fff">-</h4>
                                            <p style="color:#fff">-</p>
                                            <p style="color:#fff">-</p>
                                            <p><b style="color:#fff">|</b></p>
                                            <p><b>Quantity</b></p>
                                            <p><b><?php echo $jumlah ?></b></p>
                                        </div>
                                        <div class="col-md-3 information-entry">
                                        	<h4 style="color:#fff">-</h4>
                                            <p style="color:#fff">-</p>
                                            <p style="color:#fff">-</p>
                                            <p><b style="color:#fff">|</b></p>
                                        	<p><b>Price</b></p>
                                        	<p><b><?php echo $harga_produk ?></b></p>
                                        </div>
                                        <div class="col-md-3 information-entry">
                                        	<h4>Transfer</h4>
                                        	<p>6565057328 a.n </p>
                                        	<p>Tommy Fernandez Quiko BCA</p>
                                            <p><b>Total : Rp. <?php echo $total_harga_unik ?></b></p>
                                            <p><b>Total</b></p>
                                            <p><b><?php echo $total_harga ?></b></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>