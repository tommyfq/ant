<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
			        <div class="row">
			            <div class="col-md-12 information-entry">
			                <h3 class="block-title main-heading">Register</h3>
			                <form action="<?php echo base_url('OpenShop/new_shop') ?>" method="POST">
			                    <div class="row">
			                        <div class="col-sm-6">
			                            <label>Address <span>*</span></label>
			                            <textarea class="simple-field" placeholder="Make sure your address is correct" required name="alamat"></textarea>
			                            <label>Postal Code <span>*</span></label>
			                            <input class="simple-field" type="text" placeholder="Insert your area postal code" required name="kodepos"/>
			                            <div class="clear"></div>
			                        </div>
			                        <div class="col-sm-12">
			                            <div class="button style-10">Open Shop<input type="submit"/></div>
			                        </div>
			                    </div>
			                </form>
			                <?php if($this->session->flashdata('flashdata') != null) { ?>
			                <br />
			                <div class="row">
				                <div class="col-md-6">
					                <div class="alert alert-danger">
										<?php echo $this->session->flashdata('flashdata') ?>
									</div>
								</div>
							</div>
			                <?php } ?>
			            </div>
			        </div>
			    </div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>