<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<br />
		        <div class="row">
		        	<div class="col-sm-12 text-center">
		        		<?php if($this->session->flashdata('flashdata')) { ?>
		        		<div class="alert alert-success">
							<?php echo $this->session->flashdata('flashdata') ?>
						</div>
						<?php } ?>
		        	</div>
		        </div>
		        <div class="information-blocks">
		            <div class="row">
		            	<?php if($produk == null) { ?>
		            	<div class="col-sm-12">
		            		<div class="accordeon-entry" style="display: block;">
                                <div class="row">
                                    <div class="col-md-12 information-entry text-center">
                                        <div class="article-container style-1">
                                            <h3>You don't have any product</h3>
                                            <p>The Benefit of selling online</p>                                                
                                            <ul>
                                                <li>Fast and easy</li>
                                                <li>Very wide scopes</li>
                                            </ul>
                                            <a class="button add-product" href="<?php echo base_url('addproduct') ?>">Add Product</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
		            	</div>
		            	<?php } else { ?>
		                <div class="col-sm-12">
		                    <div class="page-selector">
		                        <div class="pages-box hidden-xs">
		                           <?php echo $links?>
		                        </div>
		                        <div class="shop-grid-controls">
		                            <div class="entry">
		                                <div class="inline-text">Sorty by</div>
		                                <div class="simple-drop-down">
		                                    <select id="myshop-sort">
		                                    	<option 
		                                    		<?php if($this->input->get('sort') == null) { echo "selected"; } ?> 
		                                    		value="">
		                                    		Choose the Order
		                                    	</option>		          								
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'mr') { echo "selected"; } ?> 
		                                        	value="<?php echo base_url('MyShop').'?sort=mr' ?>">
			                                        Most Rated
			                                    </option>
		                                        <option 
			                                        <?php if($this->input->get('sort') == 'lc') { echo "selected"; } ?> 
			                                        value="<?php echo base_url('MyShop').'?sort=lc' ?>">
			                                    	Low Cost
			                                	</option>
		                                        <option 
			                                        <?php if($this->input->get('sort') == 'hc') { echo "selected"; } ?>
			                                        value="<?php echo base_url('MyShop').'?sort=hc' ?>">
			                                    	High Cost
			                                    </option>
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'nw') { echo "selected"; } ?>
		                                        	value="<?php echo base_url('MyShop').'?sort=nw' ?>">
			                                        Newest
			                                    </option>
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'ol') { echo "selected"; } ?>
		                                        	value="<?php echo base_url('MyShop').'?sort=ol' ?>">
			                                        Oldest
			                                    </option>
		                                    </select>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="clear"></div>
		                    </div>
		                    <div class="row shop-grid list-view"> 
		                    	<?php foreach($produk as $line) { ?>
		                        <div class="col-md-3 col-sm-4 shop-grid-item">
		                            <div class="product-slide-entry shift-image">
		                                <div class="product-image">
		                                    <img style="max-height: 220px; max-width: 150px;" src="<?php echo base_url('assets/uploads/').$line['img_url']?>" alt="" />
		                                    <img src="img/product-minimal-11.jpg" alt="" />
		                                    <div class="bottom-line left-attached">
		                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
		                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
		                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
		                                        <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
		                                    </div>
		                                </div>
		                                <a class="tag" href="#"><?php echo $line['kategori_produk'] ?></a>
		                                <a class="title" href="{{url('product/detail')}}"><?php echo $line['nama_produk'] ?></a>
		                                <div class="rating-box">
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="reviews-number">25 reviews</div>
		                                </div>
		                                <div class="article-container style-1">
		                                    <p><?php echo $line['deskripsi_produk']?></p>
		                                </div>
		                                <div class="price">
		                                    <!--<div class="prev">$199,99</div>-->
		                                    <div class="current">Rp. <?php echo $line['harga_display']; ?> </div>
		                                </div>
		                                <div class="list-buttons">
		                                    <a 
		                                    data-id="<?php echo $line['id_produk']; ?>" 
		                                    data-name="<?php echo $line['nama_produk'] ?>"
		                                    data-desc="<?php echo $line['deskripsi_produk'] ?>"
		                                    data-price="<?php echo $line['harga_produk'] ?>"
		                                    data-image="<?php echo base_url('assets/uploads/').$line['img_url']?>"
		                                    class="button style-10 open-product edit">Edit Product</a>
		                                </div>
		                            </div>
		                            <div class="clear"></div>
		                        </div>
		                        <?php } ?>
		                    </div>
		                    <div class="page-selector">
		                        <div class="pages-box">
		                            <?php echo $links?>
		                        </div>
		                        <div class="clear"></div>
		                    </div>
		                </div>
		               	<?php } ?>
		            </div>
		        </div>
		    </div>
		    <br />
		    <br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
	<div id="product-popup" class="overlay-popup">
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <div class="row">
                            <div class="information-blocks">
						        <div class="row">
						            <div class="col-md-12 information-entry">
						                <h3 class="block-title main-heading">Edit Product</h3>
						                <form action="<?php echo base_url('products/editproduct') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
						                    <div class="row">
						                        <div class="col-sm-6">
						                            <label>Product Name </label>
						                            <input id="product_name" class="simple-field" type="text" placeholder="Username must be unique" name="nama_produk"/>
						                            <label>Product Description </label>
						                            <textarea id="product_desc" class="simple-field" placeholder="Your Address" name="deskripsi_produk"></textarea>
						                            <label>Product Price </label>
						                            <input id="product_price" class="simple-field" type="number" min="0" name="harga_produk"/>
						                            <label>Product Image </label>
						                            <input id="product_file" class="simple-field" type="file" name="img_url"/>
						                            <img id="product_image" style="max-height: 220px; max-width: 150px; margin-bottom: 20px" />
						                            <input id="product_id" type="hidden" name="id_produk"/>
						                            <div class="clear"></div>
						                        </div>
						                        <div class="col-sm-12">
						                            <div class="button style-10">Edit Product<input type="submit"/></div>
						                        </div>
						                    </div>
						                </form>
						            </div>
						        </div>
						    </div>
                        </div>
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include('include/foot.php'); ?>
</html>