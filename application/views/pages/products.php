<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
		        <div class="information-blocks">
		            <div class="row">
		                <div class="col-md-12">
		                    <div class="page-selector">
		                        <div class="pages-box hidden-xs">
		                            <?php echo $links?>
		                        </div>
		                        <div class="shop-grid-controls">
		                            <div class="entry">
		                                <div class="inline-text">Sorty by</div>
		                                <div class="simple-drop-down">
		                                    <select id="myshop-sort">
		                                        <option 
		                                    		<?php if($this->input->get('sort') == null) { echo "selected"; } ?> 
		                                    		value="">
		                                    		Not Ordered
		                                    	</option>		          								
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'mr') { echo "selected"; } ?> 
		                                        	value="<?php echo base_url('products').'?sort=mr' ?>">
			                                        Most Rated
			                                    </option>
		                                        <option 
			                                        <?php if($this->input->get('sort') == 'lc') { echo "selected"; } ?> 
			                                        value="<?php echo base_url('products').'?sort=lc' ?>">
			                                    	Low Cost
			                                	</option>
		                                        <option 
			                                        <?php if($this->input->get('sort') == 'hc') { echo "selected"; } ?>
			                                        value="<?php echo base_url('products').'?sort=hc' ?>">
			                                    	High Cost
			                                    </option>
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'nw') { echo "selected"; } ?>
		                                        	value="<?php echo base_url('products').'?sort=nw' ?>">
			                                        Newest
			                                    </option>
		                                        <option 
		                                        	<?php if($this->input->get('sort') == 'ol') { echo "selected"; } ?>
		                                        	value="<?php echo base_url('products').'?sort=ol' ?>">
			                                        Oldest
			                                    </option>
		                                    </select>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="clear"></div>
		                    </div>
		                    <div class="row shop-grid grid-view">
		                        <?php foreach($produk as $key) { ?>
		                        <div class="col-md-3 col-sm-4 shop-grid-item">
		                            <div class="product-slide-entry shift-image">
		                                <div class="product-image">
		                                    <img src="<?php echo base_url('assets/uploads/').$key['img_url']?>" alt="" />
		                                    <img src="img/product-minimal-11.jpg" alt="" />
		                                    <div class="bottom-line left-attached">
		                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
		                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
		                                    </div>
		                                </div>
		                                <a class="tag" href="#"><?php echo $key['kategori_produk']?></a>
		                                <a class="title" href="<?php echo base_url('products/show/').$key['id_produk'] ?>"><?php echo $key['nama_produk']?></a>
		                                <div class="rating-box">
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="star"><i class="fa fa-star"></i></div>
		                                    <div class="reviews-number">25 reviews</div>
		                                </div>
		                                <div class="article-container style-1">
		                                    <p><?php echo $key['deskripsi_produk']?></p>
		                                </div>
		                                <div class="price">
		                                    <div class="current"><?php echo $key['harga_display']?></div>
		                                </div>
		                                <div class="list-buttons">
		                                    <a class="button style-10">Add to cart</a>
		                                    <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
		                                </div>
		                            </div>
		                            <div class="clear"></div>
		                        </div>
		                        <?php } ?>
		                    </div>
		                    <div class="page-selector">
		                        <div class="pages-box">
		                            <?php echo $links?>
		                        </div>
		                        <div class="clear"></div>
		                    </div>
		                </div>
		                <!-- <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
		                    <div class="information-blocks">
		                        <div class="block-title size-2">Sort by sizes</div>
		                        <div class="range-wrapper">
		                            <div id="prices-range"></div>
		                            <div class="range-price">
		                                Price: 
		                                <div class="min-price"><b>&pound;<span>0</span></b></div>
		                                <b>-</b>
		                                <div class="max-price"><b>&pound;<span>200</span></b></div>
		                            </div>
		                            <a class="button style-14">filter</a>
		                        </div>
		                    </div>  
		                </div> -->
		            </div>
		        </div>
		    </div>
		    <br />
		    <br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>