<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<br />
		        <div class="row">
		        	<div class="col-sm-12 text-center">
		        		<?php if($this->session->flashdata('flashdata')) { ?>
		        		<div class="alert alert-success">
							<?php echo $this->session->flashdata('flashdata') ?>
						</div>
						<?php } ?>
		        	</div>
		        </div>
		        <div class="information-blocks">
		            <div class="row">
		            	<?php if($order == null) { ?>
		            	<div class="col-sm-12">
		            		<div class="accordeon-entry" style="display: block;">
                                <div class="row">
                                    <div class="col-md-12 information-entry text-center">
                                        <div class="article-container style-1">
                                            <h3>You don't have any order</h3>
                                            <p>The Benefit of selling online</p>                                                
                                            <ul>
                                                <li>Fast and easy</li>
                                                <li>Very wide scopes</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
		            	</div>
		            	<?php } else { ?>
		                <div class="information-blocks">
		                    <div class="row">
		                        <div class="col-md-9 information-entry">
		                            <div class="blog-landing-box type-3">
		                                <?php foreach($order as $key) { ?>
		                                <div class="blog-entry">
		                                    <div style="margin-left:50px" class="content">
		                                        <a class="title"><?php echo $key['nama_produk'] ?></a>
		                                        <div class="subtitle">Seller : <a><b><?php echo $key['nama_penjual'] ?></b></a></div>
		                                        <div class="subtitle">Order Date : <?php echo $key['tanggal_order'] ?> - <a><b><?php echo $key['status_order'] ?></b></a></div>
		                                        <div class="subtitle">Total Price : <a><b>Rp. <?php echo $key['total_harga'] ?></b></a></div>
		                                        <div style="margin-left:0px" class="description">Shipment Number : 
		                                        	<?php echo $key['no_resi'] == "" ? "-" : $key['no_resi'] ?> 
		                                        </div>
		                                        <?php if($key['status_order'] == "Not Paid Off") { ?>
		                                        <form action="<?php echo base_url('payment') ?>" method="POST">
		                                        	<input type="hidden" value="<?php echo $key['id_order'] ?>" name="id_order" />
	                                        		<button style="padding:0px; " class="button style-11" type="submit" >Read More</button>
	                                        	</form>
		                                        <?php } ?>
		                                    </div>
		                                </div>
		                                <?php } ?>
		                            </div>
		                            <div style="margin-left:25px;" class="page-selector">
		                                <div class="description">Showing: 1-3 of 16</div>
		                                <div class="pages-box">
		                                    <a class="square-button active" href="#">1</a>
		                                    <a class="square-button" href="#">2</a>
		                                    <a class="square-button" href="#">3</a>
		                                    <div class="divider">...</div>
		                                    <a class="square-button" href="#"><i class="fa fa-angle-right"></i></a>
		                                </div>
		                                <div class="clear"></div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		               	<?php } ?>
		            </div>
		        </div>
		    </div>
		    <br />
		    <br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>