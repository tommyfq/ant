<!-- HEADER -->
<?php include('headersetting.php'); ?>
<div class="header-wrapper style-3">
    <header class="type-1">
        <div class="header-top">
            <div class="header-top-entry hidden-xs">
                <div class="title"><i class="fa fa-eye"></i> Website Views : <?php echo $views ?> <i class="fa fa-user"></i> Website Users : <?php echo $jumlah_user_web ?></div>
            </div>
            <div class="socials-box">
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
            <div class="menu-button responsive-menu-toggle-class"><i class="fa fa-reorder"></i></div>
            <div class="clear"></div>
        </div>

        <div class="header-middle">
            <div class="logo-wrapper">
                <a id="logo" href="#"><img src="img/logo-4.png" alt="" /></a>
            </div>

            <div class="middle-entry">
                <a class="icon-entry" href="#">
                    <span class="image">
                        <i class="fa fa-check-circle"></i>
                    </span>
                    <span class="text">
                        <b>Full Collection</b> <br/> Find your academic requirement right here
                    </span>
                </a>
                <a class="icon-entry" href="#">
                    <span class="image">
                        <i class="fa fa-credit-card"></i>
                    </span>
                    <span class="text">
                        <b>Many Discount %</b> <br/> Don't worry about price
                    </span>
                </a>
            </div>

            <div class="right-entries">
                <a class="header-functionality-entry open-search-popup" href="#"><i class="fa fa-search"></i><span>Search</span></a>
                <?php if($this->session->userdata('username') == null) { ?>
                <a class="header-functionality-entry" href="<?php echo base_url('login') ?>"><i class="fa fa-sign-in"></i><span>Login</span></a>
                <?php } else { ?>
                <a class="header-functionality-entry" ><i class="fa fa-heart-o"></i><span>Wishlist</span></a>
                <a class="header-functionality-entry" href="<?php echo base_url('order') ?>"><i class="fa fa-shopping-cart"></i><span>Order</span></a>
                <a class="header-functionality-entry open-cart-popup" ><i class="fa fa-user"></i><span><?php echo $this->session->userdata('username'); ?></span>
                    <?php if($count_order > 0 ) { ?>
                    <i class="order-number"><?php echo $count_order ?></i>
                    <?php } ?>
                </a>
                <?php } ?>
                
            </div>

        </div>

        <div class="close-header-layer"></div>
        <div class="navigation">
            <div class="navigation-header responsive-menu-toggle-class">
                <div class="title">Navigation</div>
                <div class="close-menu"></div>
            </div>
            <div class="nav-overflow">
                <div class="navigation-search-content">
                    <div class="toggle-desktop-menu"><i class="fa fa-search"></i><i class="fa fa-close"></i>search</div>
                    <div class="search-box size-1">
                        <form>
                            <div class="search-button">
                                <i class="fa fa-search"></i>
                                <input type="submit" />
                            </div>
                            <div class="search-drop-down">
                                <div class="title"><span>All categories</span><i class="fa fa-angle-down"></i></div>
                                <div class="list">
                                    <div class="overflow">
                                        <div class="category-entry">Category 1</div>
                                        <div class="category-entry">Category 2</div>
                                        <div class="category-entry">Category 2</div>
                                        <div class="category-entry">Category 4</div>
                                        <div class="category-entry">Category 5</div>
                                        <div class="category-entry">Lorem</div>
                                        <div class="category-entry">Ipsum</div>
                                        <div class="category-entry">Dollor</div>
                                        <div class="category-entry">Sit Amet</div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-field">
                                <input type="text" value="" placeholder="Search for product" />
                            </div>
                        </form>
                    </div>
                </div>
                <nav>
                    <ul>
                        <li class="full-width">
                            <a href="<?php echo base_url('/') ?>" class="active">Home</a>
                        </li>
                        <li class="simple-list">
                            <a href="<?php echo base_url('products') ?>">Products</a>
                        </li>
                        <li class="column-1">
                            <a href="<?php echo base_url('AboutUs') ?>">About Us</a>
                        </li>
                        <li class="simple-list">
                            <a href="<?php echo base_url('contact') ?>">Contact</a>
                        </li>
                    </ul>

                    <div class="clear"></div>

                    <a class="fixed-header-visible additional-header-logo"><img src="img/logo-3.png" alt=""/></a>
                </nav>
                <div class="navigation-footer responsive-menu-toggle-class">
                    <div class="socials-box">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <div class="clear"></div>
                    </div>
                    <div class="navigation-copyright">Created by <a href="#">8theme</a>. All rights reserved</div>
                </div>
            </div>
        </div>
    </header>
    <div class="clear"></div>
</div>
<!--SEARCH BOX-->
<div class="search-box popup">
    <form>
        <div class="search-button">
            <i class="fa fa-search"></i>
            <input type="submit" />
        </div>
        <div class="search-drop-down">
            <div class="title"><span>All categories</span><i class="fa fa-angle-down"></i></div>
            <div class="list">
                <div class="overflow">
                    <div class="category-entry">Category 1</div>
                    <div class="category-entry">Category 2</div>
                    <div class="category-entry">Category 2</div>
                    <div class="category-entry">Category 4</div>
                    <div class="category-entry">Category 5</div>
                    <div class="category-entry">Lorem</div>
                    <div class="category-entry">Ipsum</div>
                    <div class="category-entry">Dollor</div>
                    <div class="category-entry">Sit Amet</div>
                </div>
            </div>
        </div>
        <div class="search-field">
            <input type="text" value="" placeholder="Search for product" />
        </div>
    </form>
</div>
<!--CART BOX POP UP-->
<div class="cart-box popup">
    <div class="popup-container">
        <div class="information-entry">
            <div class="detail-info-lines">
                
                <?php if($this->session->userdata('status') == null) { ?>
                <div class="share-box text-center">
                    <a href="<?php echo base_url('OpenShop') ?>"><div class="button buka-toko">Open Shop</div></a>
                </div>
                <?php } else { ?>
                <div class="share-box text-center">
                    <a href="<?php echo base_url('MyShop') ?>" class="my-account"><b>Your Shop</b></a>
                </div>
                 <div class="share-box text-center">
                    <a href="<?php echo base_url('AddProduct') ?>" class="my-account"><b>Add Product</b></a>
                </div>
                <?php } ?>
                
                <div class="share-box text-center">
                    <a href="<?php echo base_url('MyAccount') ?>" class="my-account"><b>My Account</b></a>
                </div>
                <div class="share-box text-center">
                    <a href="<?php echo base_url('login/signout')?>" class="sign-out"><i class="fa fa-sign-out"></i><b>Logout</b></a>
                </div>
            </div>
        </div>
    </div>
</div>