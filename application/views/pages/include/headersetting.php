<?php 
	$views = null;
	$this->db->select('*');
	$this->db->from('settings');
	$query = $this->db->get();
	foreach($query->result() as $key){
		$views = $key->views;
	}
	$object = [
		'views' => $views+1
	];
	$this->db->update('settings', $object);
	$jumlah_user_web = $this->db->count_all('user');
	$id_penjual_global = null;
	$count_order = null;
	$user_point = null;

	if($this->session->userdata('username')){
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('username', $this->session->userdata('username'));
		$query2 = $this->db->get();
		foreach($query2->result() as $key){
			$id_penjual_global = $key->id;
		}
		$this->db->select('count(*) as count');
		$this->db->from('pembelian');
		$this->db->where('id_penjual', $id_penjual_global);
		$query3 = $this->db->get();
		
		foreach($query3->result() as $key){
			$count_order = $key->count;
		}
	}
?>