<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
			        <div class="row">
			            <div class="col-md-8 information-entry">
			                <h3 class="block-title main-heading">Drop Us A Line</h3>
			                <form>
			                    <div class="row">
			                        <div class="col-sm-6">
			                            <label>Your Name <span>*</span></label>
			                            <input class="simple-field" type="text" placeholder="Your name (required)" required value="" />
			                            <label>Your Surname</label>
			                            <input class="simple-field" type="text" placeholder="Your surname" value="" />
			                            <label>Your Email <span>*</span></label>
			                            <input class="simple-field" type="email" placeholder="Your email address (required)" required value="" />
			                            <div class="clear"></div>
			                        </div>
			                        <div class="col-sm-12">
			                            <label>Your Message <span>*</span></label>
			                            <textarea class="simple-field" placeholder="Your message content (required)" required></textarea>
			                            <div class="button style-10">Send Message<input type="submit" value="" /></div>
			                        </div>
			                    </div>
			                </form>
			            </div>
			            <div class="col-md-4 information-entry">
			                <h3 class="block-title main-heading">Let's Stay In Touch</h3>
			                <div class="article-container style-1">
			                    <p>If you have any constraints or want to know us further, please leave a message to us</p>
			                    <h5>Company address</h5>
			                    <p>Universitas Multimedia Nusantara Scientia Boulevard Street<br/>
			                    Gading Serpong<br/>
			                    Tangerang<br/>
			                    Banten-15811<br/> 
			                    Indonesia<br/></p>
			                    <h5>Contact Informations</h5>
			                    <p>Email: stores@bookantstuff.com</p>
			                </div>
			                <div class="share-box detail-info-entry">
			                    <div class="title">Share in social media</div>
			                    <div class="socials-box">
			                        <a href="#"><i class="fa fa-instagram"></i></a>
			                    </div>
			                    <div class="clear"></div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>