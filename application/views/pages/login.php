<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
			    <div class="breadcrumb-box">
			        <a href="#">Home</a>
			        <a href="#">Login Form</a>
			    </div>

			    <div class="information-blocks">
			        <div class="row">
			            <div class="col-sm-6 information-entry">
			                <div class="login-box">
			                    <div class="article-container style-1">
			                        <h3>Registered Customers</h3>
			                        <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			                    </div>
			                    <form action="<?php echo base_url('login/signin')?>" method="POST">
			                        <label>Username</label>
			                        <input class="simple-field" type="text" placeholder="Enter Username" name="username" required/>
			                        <label>Password</label>
			                        <input class="simple-field" type="password" placeholder="Enter Password" name="password" required/>
			                        <div class="button style-10">Login Page<input type="submit" /></div>
			                    </form>
			                    <?php if($this->session->flashdata('flashdata') != null) { ?>
				                <br />
				                <div class="row">
					                <div class="alert alert-danger">
										<?php echo $this->session->flashdata('flashdata') ?>
									</div>
								</div>
				                <?php } ?>
				                </div>
			            </div>
			            <div class="col-sm-6 information-entry">
			                <div class="login-box">
			                    <div class="article-container style-1">
			                        <h3>New Customers</h3>
			                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
			                    </div>
			                    <a class="button style-12" href="<?php echo base_url('register') ?>">Register Account</a>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>