<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
			    <div class="row">
			        <div class="col-md-12">
			            <div class="information-blocks">
			                <div class="mozaic-banners-wrapper type-2">
			                    <div class="row">
			                        <div class="banner-column col-md-12">
			                            <div class="mozaic-swiper">
			                                <div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
			                                    <div class="swiper-wrapper">
			                                        <div class="swiper-slide"> 
			                                            <div class="mozaic-banner-entry type-1" style="background-image: url(img/mozaic-banner-2_1.jpg);">
			                                                <div class="mozaic-banner-content">
			                                                    <h3 class="subtitle">Imac sales</h3>
			                                                    <h2 class="title">$599,99</h2>
			                                                    <div class="description">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor.</div>
			                                                    <a class="button style-5" href="#">shop now</a>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="mozaic-banner-entry type-1" style="background-image: url(img/mozaic-banner-2_2.jpg);">
			                                                <div class="mozaic-banner-content">
			                                                    <h3 class="subtitle">sony camera</h3>
			                                                    <h2 class="title">$299,99</h2>
			                                                    <div class="description">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor.</div>
			                                                    <a class="button style-5" href="#">shop now</a>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="mozaic-banner-entry type-1" style="background-image: url(img/mozaic-banner-2_3.jpg);">
			                                                <div class="mozaic-banner-content">
			                                                    <h3 class="subtitle">AEG Blender</h3>
			                                                    <h2 class="title">$299,99</h2>
			                                                    <div class="description">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor.</div>
			                                                    <a class="button style-5" href="#">shop now</a>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="pagination"></div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>

			    <br/>
			    <br/>

			    <div class="row">
			        
			        <div class="col-md-9 col-md-push-3">
			            <div class="information-blocks">
			                <div class="tabs-container">
			                    <div class="swiper-tabs tabs-switch">
			                        <div class="title">Products</div>
			                        <div class="list">
			                            <a class="block-title tab-switcher active">Popular Products</a>
			                            <!--<a class="block-title tab-switcher">New Arrivals</a>-->
			                            <div class="clear"></div>
			                        </div>
			                    </div>
			                    <div>
			                        <div class="tabs-entry">
			                            <div class="products-swiper">
			                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="6">
			                                    <div class="swiper-wrapper">
			                                    	<?php foreach($new_product as $key){ ?>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <a class="title" href="<?php echo base_url('products/show/').$key['id_produk'] ?>"><img src="<?php echo base_url('assets/uploads/').$key['img_url']?>" alt="" /></a>
			                                                        <!-- <div class="bottom-line left-attached">
			                                                            <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
			                                                            <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
			                                                        </div> -->
			                                                    </div>
			                                                    <a class="tag"><?php echo $key['kategori_produk'] ?></a>
			                                                    <a class="title" href="<?php echo base_url('products/show/').$key['id_produk'] ?>"><?php echo $key['nama_produk']; ?></a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="current">Rp. <?php echo $key['harga_display']; ?></div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <?php } ?>
			                                    </div>
			                                    <div class="pagination"></div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="information-blocks">
			                <div class="tabs-container">
			                    <div class="swiper-tabs tabs-switch">
			                        <div class="title">Products</div>
			                        <div class="list">
			                            <!--<a class="block-title tab-switcher active">Popular Products</a>-->
			                            <a class="block-title tab-switcher active">New Arrivals</a>
			                            <div class="clear"></div>
			                        </div>
			                    </div>
			                    <div>
			                        <div class="tabs-entry">
			                            <div class="products-swiper">
			                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="6">
			                                    <div class="swiper-wrapper">
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-1.jpg" alt="" />
			                                                        <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
			                                                        <a class="top-line-a right"><i class="fa fa-heart"></i></a>
			                                                        <div class="bottom-line">
			                                                            <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-2.jpg" alt="" />
			                                                        <a class="top-line-a right open-product"><i class="fa fa-expand"></i> <span>Quick View</span></a>
			                                                        <div class="bottom-line">
			                                                            <div class="right-align">
			                                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
			                                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
			                                                            </div>
			                                                            <div class="left-align">
			                                                                <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-3.jpg" alt="" />
			                                                        <div class="bottom-line left-attached">
			                                                            <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
			                                                            <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-4.jpg" alt="" />
			                                                        <div class="bottom-line left-attached">
			                                                            <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
			                                                            <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
			                                                            <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-1.jpg" alt="" />
			                                                        <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
			                                                        <a class="top-line-a right"><i class="fa fa-heart"></i></a>
			                                                        <div class="bottom-line">
			                                                            <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="swiper-slide"> 
			                                            <div class="paddings-container">
			                                                <div class="product-slide-entry">
			                                                    <div class="product-image">
			                                                        <img src="img/product-everything-2.jpg" alt="" />
			                                                        <a class="top-line-a right open-product"><i class="fa fa-expand"></i> <span>Quick View</span></a>
			                                                        <div class="bottom-line">
			                                                            <div class="right-align">
			                                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
			                                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
			                                                            </div>
			                                                            <div class="left-align">
			                                                                <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <a class="tag" href="#">Men clothing</a>
			                                                    <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
			                                                    <div class="rating-box">
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                        <div class="star"><i class="fa fa-star"></i></div>
			                                                    </div>
			                                                    <div class="price">
			                                                        <div class="prev">$199,99</div>
			                                                        <div class="current">$119,99</div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="pagination"></div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="clear"></div>
			        </div>
			        <div class="col-md-3 col-md-pull-9 sidebar-column">
			            <div class="information-blocks">
			                <h3 class="block-title">Latest Reviews</h3>
			                <div class="swiper-container blockquote-slider" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="1">
			                    <div class="swiper-wrapper">
			                        <div class="swiper-slide"> 
			                            <blockquote class="latest-review">
			                                <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation"</div>
			                                <footer><cite>Helen Smith</cite>, 25 minutes ago</footer>
			                            </blockquote>
			                        </div>
			                        <div class="swiper-slide"> 
			                            <blockquote class="latest-review">
			                                <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation"</div>
			                                <footer><cite>Helen Smith</cite>, 25 minutes ago</footer>
			                            </blockquote>
			                        </div>
			                        <div class="swiper-slide"> 
			                            <blockquote class="latest-review">
			                                <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation"</div>
			                                <footer><cite>Helen Smith</cite>, 25 minutes ago</footer>
			                            </blockquote>
			                        </div>
			                    </div>
			                    <div class="pagination"></div>
			                </div>
			            </div>
			            <div class="information-blocks">
			                <h3 class="block-title">Company Informations</h3>
			                <div class="text-widget">
			                    <img class="image" src="img/text-widget-image-1.jpg" alt="" />
			                    <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
			                    <a class="read-more">Read more <i class="fa fa-angle-right"></i></a>
			                    <div class="clear"></div>
			                </div>
			            </div>
			            <div class="clear"></div>
			        </div>
			    </div>
			</div>
			<div class="clear"></div>
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>