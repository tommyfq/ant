<!DOCTYPE html>
<html>
<head>
    <title>ANT</title>
    <?php include('include/head.php'); ?>
    <!--<div class="loader-container">
        <div class="loader"></div>
    </div>-->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
                <span></span>
                <span id="bubble2"></span>
                <span id="bubble3"></span>
            </div>
        </div>

</head>
<body class="style-3 loaded">
    <div id="content-block">
        <div class="content-center fixed-header-margin">
        <?php include('include/header.php'); ?>
            <div class="content-push">
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-md-12 information-entry">
                            <h3 class="block-title main-heading">Register</h3>
                            <form action="<?php echo base_url('myaccount/update') ?>" method="POST">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Username <span>*</span></label>
                                        <input class="simple-field" type="text" placeholder="Username must be unique" value="<?php echo $username; ?>" required name="username" disabled/>
                                        <label>Your Email <span>*</span></label>
                                        <input class="simple-field" type="email" placeholder="example@example.com" value="<?php echo $email ?>" required name="email"/>
                                        <label>Password <span>*</span></label>
                                        <input class="simple-field" type="password" placeholder="Password" required name="password"/>
                                        <label>Phone <span>*</span></label>
                                        <input class="simple-field" type="text" placeholder="081234567890" value="<?php echo $no_hp ?>" required name="no_hp" maxlength="12" pattern="^\d{4}\d{3,4}\d{3,4}$"/>
                                        <label>Kode Pos</label>
                                        <input class="simple-field" type="text" placeholder="10000" maxlength="5" name="postal_code" value="<?php echo $postal_code ?>"/>
                                        <label>Address</label>
                                        <textarea class="simple-field" placeholder="Your Address" name="alamat" ><?php echo $alamat ?></textarea>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="button style-10">Update<input type="submit"/></div>
                                    </div>
                                </div>
                            </form>
                            <?php if($this->session->flashdata('flashdata') != null) { ?>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <?php echo $this->session->flashdata('flashdata') ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <?php include('include/footer.php'); ?>
        </div>
    </div>
</body>
<?php include('include/foot.php'); ?>
</html>

