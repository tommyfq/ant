<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
			        <div class="row">
			            <div class="col-md-12 information-entry">
			                <h3 class="block-title main-heading">Add Your Product</h3>
			                <form action="<?php echo base_url('AddProduct/new_product') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
			                    <div class="row">
			                        <div class="col-sm-6">
			                            <label>Product Name <span>*</span></label>
			                            <input class="simple-field" type="text" placeholder="Username must be unique" required name="nama_produk"/>
			                            <label>Product Description <span>*</span></label>
			                            <textarea class="simple-field" placeholder="Your Address" name="deskripsi_produk"></textarea>
			                            <label>Product Category <span>*</span></label>
			                            <div class="simple-drop-down">
			                            <select name="kategori_produk">
			                            	<option value="book">Book</option>
			                            	<option value="stuff">Stuff</option>
			                            </select>
			                        	</div>
			                            <label>Product Price <span>*</span></label>
			                            <input class="simple-field" type="number" required min="0" name="harga_produk"/>
			                            <label>Product Image <span>*</span></label>
			                            <input class="simple-field" type="file" required name="img_url"/>
			                            <input type="hidden" name="id_user" value="<?php echo $id_user; ?>"/>
			                            <div class="clear"></div>
			                        </div>
			                        <div class="col-sm-12">
			                            <div class="button style-10">Add Product<input type="submit"/></div>
			                        </div>
			                    </div>
			                </form>
			                <?php if($this->session->flashdata('error') != null) { ?>
			                <br />
			                <div class="row">
				                <div class="col-md-6">
					                <div class="alert alert-danger">
										<?php echo $this->session->flashdata('flashdata') ?>
									</div>
								</div>
							</div>
			                <?php } ?>
			            </div>
			        </div>
			    </div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>