<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
				    <img class="project-thumbnail" src="{{asset('imgs/aboutus/about-1.jpg')}}" alt="" />
				    <div class="row">
				        <div class="col-md-4 information-entry">
				            <div class="article-container style-1">
				                <h2><b>About Ant E-Commerce</b></h2>
				                <p>Ant E-Commerce is a website engaged in the sector of e-commerce that sells accounting books. Users can search accounting books that are old and up to date. Ant also provides a place for the sellers or those who have an accounting book to sell their books.</p>
                                <!--<a class="continue-link" href="#">Continue reading <i class="fa fa-long-arrow-right"></i></a>-->
				            </div>
				        </div>
				        <div class="col-md-4 information-entry">
				            <h3 class="block-title">Ant Informations</h3>
				            <div class="article-container style-1">
				                <h5><i class="fa fa-building"></i> Our Office</h5>
                                    <p>Universitas Multimedia Nusantara Scientia Boulevard Street, Gading Serpong, Tangerang, Banten-15811 Indonesia</p>
				            </div>
				            <div class="article-container style-1">
				                <h5><i class="fa fa-instagram"></i> Instagram</h5>
				                <p>bookantstuff</p>
				            </div>
				            <div class="article-container style-1">
				                <h5><i class="fa fa-wechat"></i> Line</h5>
                                    <p>http://line.me/ti/p/%40ggz8433t</p>
				            </div>
				        </div>
				        <div class="col-md-4 information-entry">
				            <div class="accordeon">
                                <div class="accordeon-title active">How to get a discount ?</div>
                                <div class="accordeon-entry" style="display: block;">
                                    <div class="article-container style-1">
                                        <p>More you buy items at <b> Ant E-commerce </b> more you can redeem your points as discounts.</p>
                                    </div>
                                </div>
				                <div class="accordeon-title ">Why should I shop at Ant E-commerce ?</div>
				                <div class="accordeon-entry">
				                    <div class="article-container style-1">
				                        <p>Ant E-commerce is a website that provides a complete selection of accounting books, from old to latest versions at an affordable price. Ant E-commerce also has a purchasing system and is easy and secure</p>
				                    </div>
				                </div>
				                <div class="accordeon-title">How to do transaction ?</div>
				                <div class="accordeon-entry">
				                    <div class="article-container style-1">
				                        <p>First select the item you want then go into process <i> checkout </i> and then it will appear total groceries, then you just make a payment then item will arrive at your place.</p>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>
			<br />
			<br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
</body>
<?php include('include/foot.php'); ?>
</html>
