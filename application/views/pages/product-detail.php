<!DOCTYPE html>
<html>
<head>
	<title>ANT</title>
	<?php include('include/head.php'); ?>
	<!--<div class="loader-container">
		<div class="loader"></div>
	</div>-->
	<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>
	
</head>
<body class="style-3 loaded">
	<div id="content-block">
	    <div class="content-center fixed-header-margin">
	        <?php include('include/header.php'); ?>
			<div class="content-push">
				<div class="information-blocks">
                    <?php if($this->session->flashdata('error') != null) { ?>
                    <br />
                    <div class="row">
                        <div class="col-md-9">
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-5 col-md-4 col-lg-5 information-entry">
                            <div style="width:100%; height:600px;" class="product-preview-box">
                                <img style="max-width: 100%; max-height: 100%;" src="<?php echo base_url('assets/uploads/').$img_url ?>" alt="" />
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-4 information-entry">
                            <div class="product-detail-box">
                                <h1 class="product-title"><?php echo $nama_produk ?></h1>
                                <h3 class="product-subtitle"><?php echo $kategori_produk ?></h3>
                                <div class="rating-box">
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star-o"></i></div>
                                    <div class="star"><i class="fa fa-star-o"></i></div>
                                    <div class="rating-number">25 Reviews</div>
                                </div>
                                <div class="product-description detail-info-entry"><?php echo $deskripsi_produk ?></div>
                                <div class="price detail-info-entry">
                                    <!-- <div class="prev">$90,00</div> -->
                                    <div class="current">Rp. <?php echo $harga_display ?></div>
                                </div>
                                <!-- <div class="quantity-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Quantity</div>
                                    <div class="entry number-minus">&nbsp;</div>
                                    <div class="entry number">1</div>
                                    <div class="entry number-plus">&nbsp;</div>
                                </div> -->
                                <div class="detail-info-entry">
                                    <a class="button style-10 open-product buy">Buy it</a>
                                    <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear visible-xs visible-sm"></div>
                        <div class="col-md-4 col-lg-3 information-entry product-sidebar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="information-blocks">
                                        <div class="information-entry products-list">
                                            <h3 class="block-title inline-product-column-title">Relevant products</h3>
                                            <?php foreach($relevant_product as $key) { ?>
                                            <div class="inline-product-entry">
                                                <a href="<?php echo base_url('products/show/').$key['id_produk'] ?>" class="image"><img style="max-width:100%; max-height:100%;" alt="" src="<?php echo base_url('assets/uploads/').$key['img_url']?>"></a>
                                                <div class="content">
                                                    <div class="cell-view">
                                                        <a href="<?php echo base_url('products/show/').$key['id_produk'] ?>" class="title"><?php echo $key['nama_produk'] ?></a>
                                                        <div class="price">
                                                            <!-- <div class="prev">$199,99</div> -->
                                                            <div class="current">Rp. <?php echo $key['harga_display'] ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="information-blocks">
                    <div class="tabs-container style-1">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title">Product info</div>
                            <div class="list">
                                <a class="tab-switcher active">Reviews (25)</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-11 information-entry">
                                            <div class="blog-landing-box type-3">
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                                <div class="blog-entry">
				                                    <div style="margin-left:0px" class="content">
				                                        <a class="title" href="#">Fresh review of coming trends for Summer '15</a>
				                                        <div class="subtitle">25 December 2015 by <a><b>Admin</b></a></div>
				                                        <div style="margin-left:0px" class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
				                                    </div>
				                                </div>
				                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <br />
            <br />
			<?php include('include/footer.php'); ?>
	    </div>
	</div>
    <div id="product-popup" class="overlay-popup">
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <div class="row">
                            <div class="information-blocks">
                                <div class="row">
                                    <?php if($this->session->userdata('username')) { ?>
                                    <div class="col-md-12 information-entry">
                                        <h3 class="block-title main-heading">Buy </h3>
                                        <form action="<?php echo base_url('order/add_order') ?>" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Product Name </label>
                                                    <div class="form-display"><?php echo $nama_produk ?></div>
                                                    <br />
                                                    <label>Product Price</label>
                                                    <div class="form-display">Rp. <?php echo $harga_display ?></div>
                                                    <br />
                                                    <div class="quantity-selector detail-info-entry">
                                                        <div class="detail-info-entry-title">Quantity</div>
                                                        <div class="entry number-minus">&nbsp;</div>
                                                        <input name="jumlah_produk" type="number" class="entry numbertype" min="0" value="1"/>
                                                        <div class="entry number-plus">&nbsp;</div>
                                                    </div>
                                                    <label>Destination Address</label>
                                                    <textarea name="alamat" class="simple-field"><?php echo $alamat ?></textarea>
                                                    <label>Destination Postal Code</label>
                                                    <input name="kode_pos" class="simple-field" type="text" value="<?php echo $postal_code ?>" />
                                                    <input type="hidden" name="id_produk" value="<?php echo $id_produk ?>"/>
                                                </div>
                                                <div style="margin-top:20px;" class="col-sm-12">
                                                    <div class="button style-10">Buy Product<input type="submit"/></div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <?php }else { ?>
                                    <div class="col-12 infomation-entry">
                                        <h3 class="block-title main-heading">Login</h3>
                                        <form action="<?php echo base_url('login/signin')?>" method="POST">
                                            <label>Username</label>
                                            <input class="simple-field" type="text" placeholder="Enter Username" name="username" required/>
                                            <label>Password</label>
                                            <input class="simple-field" type="password" placeholder="Enter Password" name="password" required/>
                                            <div class="button style-10">Login Page<input type="submit" /></div>
                                        </form>
                                        <?php if($this->session->flashdata('flashdata') != null) { ?>
                                        <br />
                                        <div class="row">
                                            <div class="alert alert-danger">
                                                <?php echo $this->session->flashdata('flashdata') ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include('include/foot.php'); ?>
</html>