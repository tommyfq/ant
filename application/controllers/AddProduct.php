<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddProduct extends CI_Controller {

	public function index()
	{
		$username = $this->session->userdata('username');
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('username', $username);
		$id_user_db = $this->db->get();
		$data = null;
		$id_user = null;
		foreach($id_user_db->result() as $key){
			$data['id_user'] = $key->id;
		}
		$this->load->view('pages/addproduct',$data);
	}

	public function new_product(){
		$nama_produk = $this->input->post('nama_produk');
		$deskripsi_produk = $this->input->post('deskripsi_produk');
		$kategori_produk = $this->input->post('kategori_produk');
		$harga_produk = $this->input->post('harga_produk');
		$new_name = date("YmdHis").$this->session->userdata('username');
		$id_user = $this->input->post('id_user');

		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000;
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;
        $config['file_name'] 			= $new_name; 
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('img_url'))
        {
            $this->session->flashdata('error', 'Something error while uploading the file, please upload again');
            redirect()->back();
        }
        else
        {
        	$upload_data = $this->upload->data();
        	$data = [
        		'id_user' => $id_user,
        		'nama_produk' => $nama_produk,
        		'kategori_produk' => $kategori_produk,
        		'deskripsi_produk' => $deskripsi_produk,
        		'img_url' => $new_name.$upload_data['file_ext'],
        		'harga_produk' => $harga_produk,
        		'tanggal_produk' => date("Y-m-d")
        	];
        	$this->db->insert('products', $data);
            redirect('MyShop');
            //$data = array('upload_data' => $this->upload->data());
                
            //$this->load->view('upload_success', $data);
        }
	}
}
