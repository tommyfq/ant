<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function index()
	{
		$sort = $this->input->get('sort');
		$page = $this->input->get('page');
		$item_per_page = 4;
		$total_rows = $this->db->count_all('products');

		$this->db->select('*');
		$this->db->from('products');
		if($sort != null){
			if($sort == "lc"){
				$this->db->order_by('harga_produk', 'asc');					
			}
			if($sort == "hc"){
				$this->db->order_by('harga_produk', 'desc');
			}
			if($sort == "nw"){
				$this->db->order_by('tanggal_produk', 'asc');	
			}
			if($sort == "ol"){
				$this->db->order_by('tanggal_produk', 'desc');
			}
			if($sort == "mr"){
				//$this->db->order_by('rating', 'desc');
			}
		}
		if (!is_null($this->input->get('page'))) { 
            if ($item_per_page != 0) { 
                $start_from = ($this->input->get('page')-1)*$item_per_page; 
                $offset = $item_per_page; 
                $this->db->limit($offset,$start_from); 
            } 
        }else{ 
            if ($item_per_page != 0) { 
                $offset = $item_per_page; 
                $this->db->limit($offset); 
            } 
        }

		//set_pagination 
        $config = array(); 
        //if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($temp_url, '', "&"); 
        $config["base_url"] = base_url(uri_string()); 
        $config['reuse_query_string'] = TRUE; 
        //$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET); 
        //$total_row = $this->db->count_all('tb_product'); 
        //echo count($total_rows); 
        //die(); 
        $config["total_rows"] = $total_rows; 
        $config["per_page"] = $item_per_page; 
        $config['use_page_numbers'] = TRUE; 
        //$config['num_links'] = $total_row; 
        $config['cur_tag_open'] = '<a class="square-button active">'; 
        $config['cur_tag_close'] = '</a>'; 
        $config['last_tag_open'] = ''; 
        $config['last_tag_close'] = ''; 
        $config['first_tag_open'] = ''; 
        $config['first_tag_close'] = ''; 
        $config['num_tag_open'] = ''; 
        $config['num_tag_close'] = ''; 
        $config['next_tag_open'] = ''; 
        $config['next_tag_close'] = ''; 
        $config['next_link'] = '<i class="fa fa-angle-right"></i>'; 
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>'; 
        $config['page_query_string'] = TRUE; 
        $config['query_string_segment'] = 'page';
        $config['attributes'] = array('class' => 'square-button');
        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['produk'] = [];
        $arr = [];
        $query = $this->db->get();
        foreach($query->result() as $key){
        	$arr['id_produk'] = $key->id_produk;
			$arr['nama_produk'] = $key->nama_produk;
			$arr['kategori_produk'] = $key->kategori_produk;
			$arr['deskripsi_produk'] = $key->deskripsi_produk;
			$arr['img_url'] = $key->img_url;
			$arr['harga_display'] = $this->spliter($key->harga_produk);
			$arr['harga_produk'] = $key->harga_produk;
			$arr['rating_produk'] = $key->rating_produk;
			$arr['tanggal_produk'] = $key->tanggal_produk;
			array_push($data['produk'], $arr);
        }
		$this->load->view('pages/products',$data);
	}

	public function show($product_id){
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id_produk', $product_id);
		$query = $this->db->get();
		$data = null;
		foreach($query->result() as $key){
			$data['id_produk'] = $key->id_produk;
			$data['nama_produk'] = $key->nama_produk;
			$data['kategori_produk'] = $key->kategori_produk;
			$data['deskripsi_produk'] = $key->deskripsi_produk;
			$data['img_url'] = $key->img_url;
			$data['harga_display'] = $this->spliter($key->harga_produk);
			$data['harga_produk'] = $key->harga_produk;
			$data['rating_produk'] = $key->rating_produk;
			$data['tanggal_produk'] = $key->tanggal_produk;

		}

		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('kategori_produk', $data['kategori_produk']);
		$this->db->where('id_produk !=',$product_id);
		$this->db->order_by('tanggal_produk', 'desc');
		$this->db->limit(3);
		$query3 = $this->db->get();

		$data['relevant_product'] = [];
		$arr = array();

		foreach($query3->result() as $key){
			$arr['id_produk'] = $key->id_produk;
			$arr['nama_produk'] = $key->nama_produk;
			$arr['kategori_produk'] = $key->kategori_produk;
			$arr['deskripsi_produk'] = $key->deskripsi_produk;
			$arr['img_url'] = $key->img_url;
			$arr['harga_display'] = $this->spliter($key->harga_produk);
			$arr['harga_produk'] = $key->harga_produk;
			$arr['rating_produk'] = $key->rating_produk;
			$arr['tanggal_produk'] = $key->tanggal_produk;
			array_push($data['relevant_product'], $arr);
		}

		// print_r($data['relevant_product']);
		// die();

		if($this->session->userdata('username')){
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('username', $this->session->userdata('username'));
			$query2 = $this->db->get();

			foreach($query2->result() as $key){
				$data['alamat'] = $key->alamat;
				$data['postal_code'] = $key->postal_code;
			}
		}

		$this->load->view('pages/product-detail',$data);
	}

	public function editproduct(){
		$id_produk = $this->input->post('id_produk');
		$nama_produk = $this->input->post('nama_produk');
		$deskripsi_produk = $this->input->post('deskripsi_produk');
		$harga_produk = $this->input->post('harga_produk');
		$new_name = date("YmdHis").$this->session->userdata('username');
		$id_user = $this->input->post('id_produk');

		$config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 3000;
        $config['max_width']            = 2048;
        $config['max_height']           = 2048;
        $config['file_name'] 			= $new_name; 
        $this->load->library('upload', $config);

        if (isset($_FILES['img_url'])) {
			if ($this->upload->do_upload('img_url'))
	        {
	           /* $this->session->flashdata('error', 'Something error while uploading the file, please upload again');
	            redirect('myshop?shp='.$this->session->userdata('username'));*/
	            $upload_data = $this->upload->data();
	        	$data = [
	        		'nama_produk' => $nama_produk,
	        		'deskripsi_produk' => $deskripsi_produk,
	        		'img_url' => $new_name.$upload_data['file_ext'],
	        		'harga_produk' => $harga_produk
	        	];

	        	$this->db->where('id_produk', $id_produk);
	        	$this->db->update('products', $data);
	        	print_r("MASUK ELSE");
	        	die();
	            redirect('MyShop');
	        }

	        $data = [
	    		'nama_produk' => $nama_produk,
	    		'deskripsi_produk' => $deskripsi_produk,
	    		'harga_produk' => $harga_produk
	    	];

	    	$this->db->where('id_produk', $id_produk);
	    	$this->db->update('products', $data);
	        redirect('MyShop');
	        
		}else{
	        $this->session->flashdata('error', 'Something error while uploading the file, please upload again');
            redirect('MyShop');
	    }
	}

	public function spliter($harga){
		$n = strlen($harga);
		$count = 0;
		$counter = 0;
		for ($i=$n; $i > 0 ; $i--) {
			$count++;
			if(($count % 3) == 1 && $count > 3){
				$counter++;
				$harga = substr($harga,0,$i).".".substr($harga,$i,$n+$counter);
			}
		}
		if(substr($harga, -1) == '.'){
			$harga = substr($harga,0,-1);
		}
		return $harga;
	}



}
