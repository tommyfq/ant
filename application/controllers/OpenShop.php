<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OpenShop extends CI_Controller {

	public function index()
	{
		$this->load->view('pages/openshop');
	}

	public function new_shop(){
		$alamat = $this->input->post('alamat');
		$kodepos = $this->input->post('kodepos');
		$username = $this->session->userdata('username');

		$data = [
			'status' => 'penjual',
			'alamat' => $alamat,
			'postal_code' => $kodepos
		];

		$this->db->where('username', $username);
		$this->db->update('user', $data);

		if($this->db->affected_rows() > 0){
			$this->session->flashdata('flashdata','Welcome to join the seller at bookantstuff');
			$this->session->unset_userdata('username');
			$this->session->set_userdata(['username' => $username, 'status' => 'penjual']);
			redirect('MyShop');
		}else{
			$this->session->flashdata('flashdata','Something wrong with the connection. Try again later');
			redirect()->back();
		}
	}
}
