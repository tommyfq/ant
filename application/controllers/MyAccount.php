<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class MyAccount extends CI_Controller {
        
        public function index()
        {
            $username = $this->session->userdata('username');
            if($username == null){
                redirect('/');
            }else{
                $this->db->select('*');
                $this->db->from('user');
                $this->db->where('username', $username);
                $query = $this->db->get();
                
                $data = array();
                if(count($query->result()) > 0){
                    foreach ($query->result() as $key) {
                        $data['username'] = $key->username;
                        $data['alamat'] = $key->alamat;
                        $data['no_hp'] = $key->no_hp;
                        $data['email'] = $key->email;
                        $data['postal_code'] = $key->postal_code;
                    }
                }
                $this->load->view('pages/myaccount', $data);
            }
        }
        
        public function update(){
            $username = $this->session->userdata('username');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $no_hp = $this->input->post('no_hp');
            $alamat = $this->input->post('alamat');
            $postal_code = $this->input->post('postal_code');
            
            $data = [
            'password' => md5($password),
            'email' => $email,
            'no_hp' => $no_hp,
            'alamat' => $alamat,
            'postal_code' => $postal_code
            ];
            $this->db->where('username', $username);
            $this->db->update('user', $data);
            redirect('myaccount?shp='.$username);
            
        }
        
    }
