<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->db->select('*');
		$this->db->from('products');
		$this->db->order_by('tanggal_produk', 'desc');
		$this->db->limit(5);
		$query = $this->db->get();

		$data['new_product'] =  [];
		
		$arr = array();
		foreach($query->result() as $key){
			$arr['id_produk'] = $key->id_produk;
			$arr['nama_produk'] = $key->nama_produk;
			$arr['kategori_produk'] = $key->kategori_produk;
			$arr['deskripsi_produk'] = $key->deskripsi_produk;
			$arr['img_url'] = $key->img_url;
			$arr['harga_display'] = $this->spliter($key->harga_produk);
			$arr['harga_produk'] = $key->harga_produk;
			$arr['rating_produk'] = $key->rating_produk;
			$arr['tanggal_produk'] = $key->tanggal_produk;
			array_push($data['new_product'], $arr);
		}

		$this->db->select('*');
		$this->db->from('products');
		$this->db->order_by('rating_produk', 'desc');
		$this->db->limit(5);
		$query = $this->db->get();

		$data['top_product'] =  [];
		
		$arr = array();
		foreach($query->result() as $key){
			$arr['id_produk'] = $key->id_produk;
			$arr['nama_produk'] = $key->nama_produk;
			$arr['kategori_produk'] = $key->kategori_produk;
			$arr['deskripsi_produk'] = $key->deskripsi_produk;
			$arr['img_url'] = $key->img_url;
			$arr['harga_display'] = $this->spliter($key->harga_produk);
			$arr['harga_produk'] = $key->harga_produk;
			$arr['rating_produk'] = $key->rating_produk;
			$arr['tanggal_produk'] = $key->tanggal_produk;
			array_push($data['top_product'], $arr);
		}
		$this->load->view('pages/home',$data);
	}

	public function spliter($harga){
		$n = strlen($harga);
		$count = 0;
		$counter = 0;
		for ($i=$n; $i > 0 ; $i--) {
			$count++;
			if(($count % 3) == 1 && $count > 3){
				$counter++;
				$harga = substr($harga,0,$i).".".substr($harga,$i,$n+$counter);
			}
		}
		if(substr($harga, -1) == '.'){
			$harga = substr($harga,0,-1);
		}
		return $harga;
	}
}
