<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index()
	{
		$this->load->view('pages/register');
	}

	public function new_user(){
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$no_hp = $this->input->post('no_hp');
		if($this->input->post('alamat') != null){
			$alamat = $this->input->post('alamat');
		}else{
			$alamat = "";
		}
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username',$username);
		$user_cek = $this->db->get();

		$email_db = "";
		$no_hp_db = "";

		foreach ($user_cek->result() as $key) {
			$email_db = $key->email;
			$no_hp_db = $key->no_hp;
		}

		$flashdata = "";

		if(count($user_cek->result()) > 0){
			$flashdata .= "Username";
		}
		if($email_db == $email){
			$flashdata .= ", Email";
		}
		if($no_hp_db == $no_hp){
			$flashdata .= ", Phone Number";
		}
		if($email_db != $email && $no_hp_db != $no_hp && count($user_cek->result()) < 1){

			$data = [
				'username' => $username,
				'email' => $email,
				'password' => md5($password),
				'no_hp' => $no_hp,
				'alamat' => $alamat,
				'status_kode_promo' => 'n'
			];
			$this->db->insert('user', $data);
			$this->session->set_userdata(['username' => $username, 'status' => null]);
			redirect('home');
		}

		$session_flashdata = array(
			'flashdata' => $flashdata .= " is already used !"
		);

		$this->session->set_flashdata($session_flashdata);

		redirect('register');

	}

}
