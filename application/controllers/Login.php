<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('pages/login');
	}

	public function signin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $username);
		$user_cek = $this->db->get();

		$username_db = "";
		$password_db = "";
		$status_db = "";

		foreach ($user_cek->result() as $key) {
			$username_db = $key->usernname;
			$password_db = $key->password;
			$status_db = $key->status;
		}
		
		if($username_db != $username && $password_db != md5($password)){
			$this->session->set_flashdata('flashdata', 'Your Username or Password is incorrect');
			redirect('login');
		}
		if($this->session->userdata('username') == $username){
			$this->session->set_flashdata('flashdata', 'Your account is already login');
			redirect('login');
		}else{
			$this->session->set_userdata(['username' => $username, 'status' => $status_db]);
			redirect()->back();
		}
	}

	public function signout(){
		$this->session->unset_userdata('username');
		redirect()->back();
	}

}
