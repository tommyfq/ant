<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function index()
	{
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('username', $this->session->userdata('username'));
		$query = $this->db->get();
		$id_user = null;
		foreach ($query->result() as $key) {
			$id_user = $key->id;
		}

		$this->db->select('pe.id_order, pe.jumlah, pe.status_order, pe.tanggal_order, u.username, po.harga_produk, pe.no_resi_pengiriman, po.nama_produk');
		$this->db->from('pembelian as pe');
		$this->db->join('user as u', 'u.id = pe.id_penjual');
		$this->db->join('products as po', 'po.id_produk = pe.id_produk');
		$this->db->where('pe.id_user', $id_user);
		$query2 = $this->db->get();

		/*print_r($query2->result());
		die();*/

		$data['order'] = [];
		$arr = [];
		foreach($query2->result() as $key){
			$arr['id_order'] = $key->id_order;
			$arr['nama_produk'] = $key->nama_produk;
			$arr['nama_penjual'] = $key->username;
			$arr['total_harga'] = $this->spliter((string) $this->calc($key->jumlah, $key->harga_produk));
			$arr['status_order'] = $key->status_order;
			$arr['no_resi'] = $key->no_resi_pengiriman;
			$arr['tanggal_order'] = $key->tanggal_order;
			array_push($data['order'], $arr);
		}

		$this->load->view('pages/myorders',$data);
	}

	public function add_order(){
		$jumlah_produk = $this->input->post("jumlah_produk");
		$tanggal_order = date('Y-m-d');
		$id_produk = $this->input->post("id_produk");
		$alamat = $this->input->post('alamat');
		$kode_pos = $this->input->post('kode_pos');


		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('id_produk', $id_produk);
		$query = $this->db->get();
		
		$id_penjual = null;
		foreach($query->result() as $key){
			$id_penjual = $key->id_user;
		}

		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username', $this->session->userdata('username'));
		$query2 = $this->db->get();

		$id_user = null;
		$alamat_user = null;
		$postal_code_user = null;
		foreach($query2->result() as $key){
			$id_user = $key->id;
			$alamat_user = $key->alamat;
			$postal_code_user = $key->postal_code;
		}

		if($id_penjual == $id_user){
			$this->session->flashdata('error', "You can't buy your own product");
            redirect('products/show/'.$id_produk);
		}else{
			$data = [
				'id_produk' => $id_produk,
				'id_user' => $id_user,
				'id_penjual' => $id_penjual,
				'jumlah' => $jumlah_produk,
				'status_order' => 'Not Paid Off',
				'tanggal_order' => $tanggal_order,
				'nomor_unik' => $this->getRandomNum()
			];
		}

		if($alamat != $alamat_user){
			$data = [
				'alamat_tujuan' => $alamat
			];
		}

		if($kode_pos != $postal_code_user){
			$data = [
				'postal_code_tujuan' => $kode_pos
			];
		}
	
		$this->db->select('id_order');
		$this->db->from('pembelian');
		$this->db->where('id_produk', $id_produk);
		$this->db->where('id_user', $id_user);
		$this->db->where('id_penjual', $id_penjual);
		$this->db->where('tanggal_order', $tanggal_order);
		$query3 = $this->db->get();
		if(count($query3->result()) > 0){
			$this->session->set_flashdata('error', "You can't order the same product in one day");
			redirect('products/show/'.$id_produk);
		}else{
			/*$object = [
				'poin' => 
			];
			$this->db->where('$id', $id_user);
			$this->db->update('user', $object);*/

			$this->db->insert('pembelian', $data);			
			$this->session->set_userdata($data);
			redirect('payment');
		}
	}

	public function calc($j, $h){
		$jumlah = (int) $j;
		$harga = (int) $h;
		return $jumlah*$harga;
	}

	public function spliter($harga){
		$n = strlen($harga);
		$count = 0;
		$counter = 0;
		for ($i=$n; $i > 0 ; $i--) {
			$count++;
			if(($count % 3) == 1 && $count > 3){
				$counter++;
				$harga = substr($harga,0,$i).".".substr($harga,$i,$n+$counter);
			}
		}
		if(substr($harga, -1) == '.'){
			$harga = substr($harga,0,-1);
		}
		return $harga;
	}

	public function getRandomNum(){
		$digits = 3;
		return rand(pow(10, $digits-1), pow(10, $digits)-1);
	}

}