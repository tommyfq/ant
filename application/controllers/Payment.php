<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('id_produk')){
			$id_produk = $this->session->userdata('id_produk');
			$id_user = $this->session->userdata('id_user');
			$id_penjual = $this->session->userdata('id_penjual');
			$tanggal_order = $this->session->userdata('tanggal_order');

			$this->db->select('pe.jumlah, pe.tanggal_order, u.username, po.harga_produk, pe.no_resi_pengiriman, po.nama_produk, u.alamat, pe.nomor_unik');
			$this->db->from('pembelian as pe');
			$this->db->join('user as u', 'u.id = pe.id_penjual');
			$this->db->join('products as po', 'po.id_produk = pe.id_produk');
			$this->db->where('pe.id_user', $id_user);
			$this->db->where('pe.id_produk', $id_produk);
			$this->db->where('pe.id_penjual', $id_penjual);
			$this->db->where('pe.tanggal_order', $tanggal_order);
			$query = $this->db->get();

		}else if($this->input->post('id_order') != null){
			$id_order = $this->input->post('id_order');
			$this->db->select('pe.jumlah, pe.tanggal_order, u.username, po.harga_produk, po.nama_produk, u.alamat, pe.nomor_unik');
			$this->db->from('pembelian as pe');
			$this->db->join('user as u', 'u.id = pe.id_penjual');
			$this->db->join('products as po', 'po.id_produk = pe.id_produk');
			$this->db->where('pe.id_order', $id_order);
			$query = $this->db->get();

		}else{
			redirect('/');
		}

		foreach($query->result() as $key){
			$harga_produk = $this->addRandom($key->harga_produk, $key->nomor_unik);
			$data['nama_produk'] = $key->nama_produk;
			$data['jumlah'] = $key->jumlah;
			$data['tanggal_order'] = $key->tanggal_order;
			$data['username'] = $key->username;
			$data['harga_produk'] = $this->spliter($key->harga_produk);
			$data['alamat'] = $key->alamat; 
			$data['total_harga_unik'] = $this->spliter((string) $this->calc($key->jumlah, $harga_produk));
			$data['total_harga'] = $this->spliter((string) $this->calc($key->jumlah, $key->harga_produk));
		}

		$this->load->view('pages/payment',$data);
	}

	public function calc($j, $h){
		$jumlah = (int) $j;
		$harga = (int) $h;
		return $jumlah*$harga;
	}

	public function spliter($harga){
		$n = strlen($harga);
		$count = 0;
		$counter = 0;
		for ($i=$n; $i > 0 ; $i--) {
			$count++;
			if(($count % 3) == 1 && $count > 3){
				$counter++;
				$harga = substr($harga,0,$i).".".substr($harga,$i,$n+$counter);
			}
		}
		if(substr($harga, -1) == '.'){
			$harga = substr($harga,0,-1);
		}
		return $harga;
	}

	public function addRandom($harga, $no_unik){
		$nomor_unik = (int) $no_unik;
		$harga_unik = (int) $harga;
		return $harga_unik + $nomor_unik;
	}

}