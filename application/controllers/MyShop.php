<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyShop extends CI_Controller {

	public function index()
	{
		$sort = $this->input->get('sort');
		$page = $this->input->get('page');
		$item_per_page = 4;
		$username = $this->session->userdata('username');
		//check if username
		if($username != null){
			$this->db->select('id');
			$this->db->from('user');
			$this->db->where('username', $username);
			$query_user = $this->db->get();

			$id_user = null;
			foreach ($query_user->result() as $key) {
				$id_user = $key->id;
			}

			$this->db->where('id_user', $id_user);
			$total_rows = $this->db->count_all('products');

			$this->db->select('*');
			$this->db->from('products');
			$this->db->join('user', 'products.id_user = user.id');
			$this->db->where('id_user',$id_user);

			if($sort != null){
				if($sort == "lc"){
					$this->db->order_by('harga_produk', 'asc');					
				}
				if($sort == "hc"){
					$this->db->order_by('harga_produk', 'desc');
				}
				if($sort == "nw"){
					$this->db->order_by('tanggal_produk', 'asc');	
				}
				if($sort == "ol"){
					$this->db->order_by('tanggal_produk', 'desc');
				}
				if($sort == "mr"){
						
				}
				
			}

			if (!is_null($this->input->get('page'))) { 
	            if ($item_per_page != 0) { 
	                $start_from = ($this->input->get('page')-1)*$item_per_page; 
	                $offset = $item_per_page; 
	                $this->db->limit($offset,$start_from); 
	            } 
	        }else{ 
	            if ($item_per_page != 0) { 
	                $offset = $item_per_page; 
	                $this->db->limit($offset); 
	            } 
	        }
	        
	        //set_pagination 
	        $config = array(); 
	        //if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($temp_url, '', "&"); 
	        $config["base_url"] = base_url(uri_string()); 
	        $config['reuse_query_string'] = TRUE; 
	        //$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET); 
	        //$total_row = $this->db->count_all('tb_product'); 
	        //echo count($total_rows); 
	        //die(); 
	        $config["total_rows"] = $total_rows; 
	        $config["per_page"] = $item_per_page; 
	        $config['use_page_numbers'] = TRUE; 
	        //$config['num_links'] = $total_row; 
	        $config['cur_tag_open'] = '<a class="square-button active">'; 
	        $config['cur_tag_close'] = '</a>'; 
	        $config['last_tag_open'] = ''; 
	        $config['last_tag_close'] = ''; 
	        $config['first_tag_open'] = ''; 
	        $config['first_tag_close'] = ''; 
	        $config['num_tag_open'] = ''; 
	        $config['num_tag_close'] = ''; 
	        $config['next_tag_open'] = ''; 
	        $config['next_tag_close'] = ''; 
	        $config['next_link'] = '<i class="fa fa-angle-right"></i>'; 
	        $config['prev_link'] = '<i class="fa fa-angle-left"></i>'; 
	        $config['page_query_string'] = TRUE; 
	        $config['query_string_segment'] = 'page';
	        $config['attributes'] = array('class' => 'square-button');
	        $this->pagination->initialize($config);

	        $data["links"] = $this->pagination->create_links();

	        $data['produk'] = [];
	        $arr = [];
	        $query = $this->db->get();
			if(count($query->result()) > 0){
				$data['produk'] = [];
				$arr = array();
				foreach ($query->result() as $key) {
					$arr['id_produk'] = $key->id_produk;
					$arr['nama_produk'] = $key->nama_produk;
					$arr['kategori_produk'] = $key->kategori_produk;
					$arr['deskripsi_produk'] = $key->deskripsi_produk;
					$arr['img_url'] = $key->img_url;
					$arr['harga_display'] = $this->spliter($key->harga_produk);
					$arr['harga_produk'] = $key->harga_produk;
					$arr['rating_produk'] = $key->rating_produk;
					$arr['tanggal_produk'] = $key->tanggal_produk;
					array_push($data['produk'], $arr);
				}
			}
			$this->load->view('pages/myshop',$data);
		}else{
			redirect('/');
		}
	}

	public function spliter($harga){
		$n = strlen($harga);
		$count = 0;
		$counter = 0;
		for ($i=$n; $i > 0 ; $i--) {
			$count++;
			if(($count % 3) == 1 && $count > 3){
				$counter++;
				$harga = substr($harga,0,$i).".".substr($harga,$i,$n+$counter);
			}
		}
		if(substr($harga, -1) == '.'){
			$harga = substr($harga,0,-1);
		}
		return $harga;
	}

}
